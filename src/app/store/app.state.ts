import { ITodo } from '../models/itodo.model';
import { AddTodo, RemoveTodo } from './app.actions';
import { Action, Selector, State, StateContext } from '@ngxs/store';

interface IAppState {
    todoList: ITodo[]
}

@State<IAppState>({
    name: 'app',
    defaults: { todoList: [] }
})

export class AppState {
    @Selector()
    static todoList(state: IAppState) {
        return state.todoList
    }

    @Action(AddTodo)
    addTodo(ctx: StateContext<IAppState>, action: AddTodo) {
        const { todo } = action
        const { todoList } = ctx.getState()
        ctx.patchState({
            todoList: [...todoList, todo],
        });
    }

    @Action(RemoveTodo)
    removeTodo(ctx: StateContext<IAppState>, action: RemoveTodo) {
        const { id } = action
        const { todoList } = ctx.getState()
        ctx.patchState({
            todoList: todoList.filter(todo => todo.id != id)
        });
    }
}