import { ITodo } from '../models/itodo.model';

export class AddTodo {
    static readonly type = '[Todo] Add Todo';
    constructor(public todo: ITodo) { }
}

export class RemoveTodo {
    static readonly type = '[Todo] Remove Todo';
    constructor(public id: string) { }
}