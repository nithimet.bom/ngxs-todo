import { AddTodo, RemoveTodo } from './store/app.actions';
import { ITodo } from './models/itodo.model';
import { AppState } from './store/app.state';
import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    @Select(AppState.todoList) todoList!: Observable<ITodo[]>

    constructor(private store: Store) { }

    onAddTodo(title: string) {
        this.store.dispatch(new AddTodo({ id: Date.now().toString(), title }))
    }

    onRemoveTodo(id: string) {
        this.store.dispatch(new RemoveTodo(id))
    }
}
